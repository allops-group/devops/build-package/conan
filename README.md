# Conan

This project is put on hold due to [lack of support for Conan 2.0](https://gitlab.com/gitlab-org/gitlab/-/issues/389216) on GitLab. Uploading package to package registry leads to error

> ERROR: The remote doesn't support revisions. Conan 2.0 is no longer compatible with remotes that don't accept revisions.. [Remote: gitlab]

Further investigation regarding alternatives (e.g. [Conan Center](https://conan.io/center)) is required.