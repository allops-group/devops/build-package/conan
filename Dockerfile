FROM debian:bookworm-slim
ENV DEBIAN_FRONTEND noninteractive

LABEL maintainer="Aleksandar Atanasov aleksandar.vl.atanasov@gmail.com"

RUN apt-get update && \
    apt-get install -q -y --no-install-recommends build-essential cmake python3-full python3-pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --no-cache-dir conan
